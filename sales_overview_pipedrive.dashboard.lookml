- dashboard: sales_overview_pipedrive
  title: Sales Overview Pipedrive
  layout: newspaper
  elements:
  - name: Opportunities
    type: text
    title_text: Opportunities
    row: 13
    col: 0
    width: 24
    height: 2
  - title: New Contacts (Total)
    name: New Contacts (Total)
    model: crm_pipedrive
    explore: contact
    type: single_value
    fields: [contact.count]
    filters:
      contact.contact_type: Contact
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 32
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - contact.count",
            id: " - contact.count", name: Contact, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 36}, {axisId: Direct - contact.count, id: Direct - contact.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 37}, {axisId: Drift - contact.count, id: Drift - contact.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 38}, {axisId: Employee Referral - contact.count, id: Employee
              Referral - contact.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 39}, {axisId: Event - contact.count, id: Event - contact.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 41}, {axisId: EventBrite - contact.count, id: EventBrite -
              contact.count, name: EventBrite, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 42}, {axisId: gmail - contact.count, id: gmail - contact.count,
            name: gmail, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 43}, {axisId: Inbound - contact.count, id: Inbound - contact.count,
            name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 44}, {axisId: Mailchimp - contact.count, id: Mailchimp - contact.count,
            name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 45}, {axisId: Other - contact.count, id: Other - contact.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 46}, {axisId: Partner - contact.count, id: Partner - contact.count,
            name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 47}, {axisId: Social Media - contact.count, id: Social Media
              - contact.count, name: Social Media, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 48}, {axisId: Zoominfo - contact.count, id: Zoominfo - contact.count,
            name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 49}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 36}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    label_color: []
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    show_null_points: true
    interpolation: linear
    listen:
      Date: contact.date_created_date
      Lead Source: contact.lead_source
    row: 4
    col: 0
    width: 24
    height: 2
  - title: New Contacts
    name: New Contacts
    model: crm_pipedrive
    explore: contact
    type: looker_column
    fields: [contact.date_created_month, contact.lead_source, contact.count]
    pivots: [contact.lead_source]
    fill_fields: [contact.date_created_month]
    filters:
      contact.contact_type: Contact
    sorts: [contact.date_created_month desc, contact.lead_source]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 242
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - contact.count",
            id: " - contact.count", name: Contact, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 246}, {axisId: Direct - contact.count, id: Direct - contact.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 247}, {axisId: Drift - contact.count, id: Drift - contact.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 248}, {axisId: Employee Referral - contact.count, id: Employee
              Referral - contact.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 249}, {axisId: Event - contact.count, id: Event - contact.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 251}, {axisId: EventBrite - contact.count, id: EventBrite
              - contact.count, name: EventBrite, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 252}, {axisId: gmail - contact.count, id: gmail - contact.count,
            name: gmail, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 253}, {axisId: Inbound - contact.count, id: Inbound - contact.count,
            name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 254}, {axisId: Mailchimp - contact.count, id: Mailchimp -
              contact.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 255}, {axisId: Other - contact.count, id: Other - contact.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 256}, {axisId: Partner - contact.count, id: Partner - contact.count,
            name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 257}, {axisId: Social Media - contact.count, id: Social Media
              - contact.count, name: Social Media, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 258}, {axisId: Zoominfo - contact.count, id: Zoominfo - contact.count,
            name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 259}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 246}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    label_color: []
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    show_null_points: true
    interpolation: linear
    listen:
      Date: contact.date_created_date
      Lead Source: contact.lead_source
    row: 6
    col: 0
    width: 24
    height: 7
  - title: New Opportunities
    name: New Opportunities
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.date_created_month, opportunity.lead_source, opportunity.count]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_created_month]
    sorts: [opportunity.date_created_month desc, opportunity.lead_source]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 311
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 315}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 316}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 318}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 319}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 320}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 321}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 323}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 324}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 325}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 326}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 327}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 329}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 315}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_created_date
      Lead Source: opportunity.lead_source
    row: 17
    col: 0
    width: 12
    height: 6
  - title: New Opportunities (Total)
    name: New Opportunities (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.count]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 375
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 379}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 380}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 382}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 383}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 384}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 385}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 387}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 388}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 389}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 390}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 391}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 393}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 379}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_created_date
      Lead Source: opportunity.lead_source
    row: 15
    col: 0
    width: 12
    height: 2
  - title: New Opportunities Value (Total)
    name: New Opportunities Value (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.opportunity_value]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 439
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 443}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 444}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 446}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 447}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 448}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 449}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 451}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 452}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 453}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 454}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 455}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 457}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 443}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_created_date
      Lead Source: opportunity.lead_source
    row: 15
    col: 12
    width: 12
    height: 2
  - title: New Opportunities Value
    name: New Opportunities Value
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.date_created_month, opportunity.lead_source, opportunity.opportunity_value]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_created_month]
    sorts: [opportunity.date_created_month desc, opportunity.lead_source]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 506
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 510}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 511}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 513}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 514}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 515}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 516}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 518}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 519}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 520}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 521}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 522}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 524}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 510}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_created_date
      Lead Source: opportunity.lead_source
    row: 17
    col: 12
    width: 12
    height: 6
  - title: Won Opportunities
    name: Won Opportunities
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.lead_source, opportunity.count, opportunity.date_closed_month]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_closed_month]
    filters:
      opportunity.is_won: 'Yes'
    sorts: [opportunity.lead_source]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 575
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 579}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 580}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 582}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 583}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 584}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 585}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 587}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 588}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 589}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 590}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 591}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 593}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 579}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 25
    col: 0
    width: 12
    height: 6
  - title: Won Opportunities (Total)
    name: Won Opportunities (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.count]
    filters:
      opportunity.is_won: 'Yes'
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 641
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 645}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 646}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 648}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 649}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 650}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 651}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 653}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 654}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 655}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 656}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 657}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 659}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 645}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 23
    col: 0
    width: 12
    height: 2
  - title: Won Opportunities Value (Total)
    name: Won Opportunities Value (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.opportunity_value]
    filters:
      opportunity.is_won: 'Yes'
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 707
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 711}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 712}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 714}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 715}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 716}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 717}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 719}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 720}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 721}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 722}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 723}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 725}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 711}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 23
    col: 12
    width: 12
    height: 2
  - title: Won Opportunities Value
    name: Won Opportunities Value
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.lead_source, opportunity.date_closed_month, opportunity.opportunity_value]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_closed_month]
    filters:
      opportunity.is_won: 'Yes'
    sorts: [opportunity.lead_source, opportunity.date_closed_month desc]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 776
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 780}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 781}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 783}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 784}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 785}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 786}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 788}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 789}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 790}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 791}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 792}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 794}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 780}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 25
    col: 12
    width: 12
    height: 6
  - title: Lost Opportunities (Total)
    name: Lost Opportunities (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.count]
    filters:
      opportunity.is_won: 'No'
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 842
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 846}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 847}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 849}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 850}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 851}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 852}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 854}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 855}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 856}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 857}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 858}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 860}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 846}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 31
    col: 0
    width: 12
    height: 2
  - title: Lost Opportunities Value (Total)
    name: Lost Opportunities Value (Total)
    model: crm_pipedrive
    explore: opportunity
    type: single_value
    fields: [opportunity.opportunity_value]
    filters:
      opportunity.is_won: 'No'
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 908
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 912}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 913}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 915}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 916}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 917}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 918}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 920}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 921}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 922}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 923}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 924}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 926}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 912}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 31
    col: 12
    width: 12
    height: 2
  - title: Lost Opportunities
    name: Lost Opportunities
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.lead_source, opportunity.count, opportunity.date_closed_month]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_closed_month]
    filters:
      opportunity.is_won: 'No'
    sorts: [opportunity.lead_source, opportunity.date_closed_month desc]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 977
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 981}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 982}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 984}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 985}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 986}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 987}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 989}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 990}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 991}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 992}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 993}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 995}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 981}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 33
    col: 0
    width: 12
    height: 6
  - title: Lost Opportunities Value
    name: Lost Opportunities Value
    model: crm_pipedrive
    explore: opportunity
    type: looker_column
    fields: [opportunity.lead_source, opportunity.date_closed_month, opportunity.opportunity_value]
    pivots: [opportunity.lead_source]
    fill_fields: [opportunity.date_closed_month]
    filters:
      opportunity.is_won: 'No'
    sorts: [opportunity.lead_source, opportunity.date_closed_month desc]
    limit: 500
    color_application:
      collection_id: legacy
      palette_id: santa_cruz
      options:
        steps: 5
        __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml
        __LINE_NUM: 1046
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: " - opportunity.count",
            id: " - opportunity.count", name: Opportunity, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1050}, {axisId: Client Referral - opportunity.count, id: Client
              Referral - opportunity.count, name: Client Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1051}, {axisId: Direct - opportunity.count, id: Direct - opportunity.count,
            name: Direct, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1053}, {axisId: Drift - opportunity.count, id: Drift - opportunity.count,
            name: Drift, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1054}, {axisId: Employee Referral - opportunity.count, id: Employee
              Referral - opportunity.count, name: Employee Referral, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1055}, {axisId: Event - opportunity.count, id: Event - opportunity.count,
            name: Event, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1056}, {axisId: Inbound - opportunity.count, id: Inbound -
              opportunity.count, name: Inbound, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1058}, {axisId: Mailchimp - opportunity.count, id: Mailchimp
              - opportunity.count, name: Mailchimp, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1059}, {axisId: Other - opportunity.count, id: Other - opportunity.count,
            name: Other, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1060}, {axisId: Partner - opportunity.count, id: Partner -
              opportunity.count, name: Partner, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1061}, {axisId: PlugNPlay - opportunity.count, id: PlugNPlay
              - opportunity.count, name: PlugNPlay, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1062}, {axisId: Zoominfo - opportunity.count, id: Zoominfo
              - opportunity.count, name: Zoominfo, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
            __LINE_NUM: 1064}], showLabels: false, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear, __FILE: crm_pipedrive/sales_overview_pipedrive.dashboard.lookml,
        __LINE_NUM: 1050}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#353b49"
    listen:
      Date: opportunity.date_closed_date
      Lead Source: opportunity.lead_source
    row: 33
    col: 12
    width: 12
    height: 6
  - name: 'scaffold'
    type: text
    subtitle_text: <font size="5px"><font color="#408ef7"><b>Pipedrive - CRM</b></font>
    row: 0
    col: 0
    width: 7
    height: 2
  - name: 'dashboard'
    type: text
    subtitle_text: <font size="5px"><font color="#408ef7"><b>Overview</b></font>
    row: 0
    col: 7
    width: 11
    height: 2
  - name: 'powered by'
    type: text
    body_text: <a href="https://keboola.com" target="_blank"> <img src="https://keboola-resources.s3.amazonaws.com/poweredByKeboola.png"
      width="100%"/>
    row: 0
    col: 18
    width: 6
    height: 2
  - name: Contacts
    type: text
    title_text: Contacts
    row: 2
    col: 0
    width: 24
    height: 2
  filters:
  - name: Date
    title: Date
    type: date_filter
    default_value: 6 months
    allow_multiple_values: true
    required: false
  - name: Lead Source
    title: Lead Source
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: crm_pipedrive
    explore: opportunity
    listens_to_filters: []
    field: opportunity.lead_source
